import cherrypy
from bson import json_util

from .. import decos

from ..resources.users import extract_balance, get_user, update_user_balance, \
    UserDoesNotExistException, UserNegativeBalanceException


class Balance(object):
    exposed = True

    @decos.standard_web_handler
    def GET(self, username):
        return extract_balance(
            get_user(username, must_exist=True)
        )

    @decos.standard_web_handler
    def POST(self, username):
        delta = cherrypy.request.json['delta']

        return update_user_balance(
            get_user(username, must_exist=True),
            delta
        )
