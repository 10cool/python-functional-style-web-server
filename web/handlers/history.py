import cherrypy
from bson import json_util

from .. import decos

from ..resources.users import extract_balance, get_user, update_user_balance, \
    extract_history, UserDoesNotExistException, UserNegativeBalanceException


class History(object):
    exposed = True

    @decos.standard_web_handler
    def GET(self, username):
        return extract_history(
                get_user(username, must_exist=True))
