import cherrypy
from bson import json_util
import random

from .. import decos

from ..resources.users import extract_balance, get_user, update_user_balance, \
    create_wager, UserDoesNotExistException, UserNegativeBalanceException

from .. import dbapplogic


class Wagers(object):
    exposed = True

    @decos.standard_web_handler
    def POST(self, username):
        wager = cherrypy.request.json['wager']
        create_wager(
            get_user(username, must_exist=True),
            wager
        )

        delta = wager
        if random.random() > 0.5:
            delta = -(wager)

        return update_user_balance(
            get_user(username, must_exist=True),
            delta
        )
