from .resources.users import UserDoesNotExistException, UserNegativeBalanceException, \
    UserBalanceInconsistentException


def standard_web_handler(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except UserDoesNotExistException as e:
            return {"error": "user %s does not exist" % e.username}
        except UserNegativeBalanceException as e:
            return {"error": "user %s does not have the funds for amt %s" %
                    (e.username, e.amt)}
        except UserBalanceInconsistentException as e:
            return {"error": "Attempted to modify balance for user %s but "
                    "cached balance of %s did not match value store in the "
                    "database" %
                    (e.username, e.cached_balance)}

    return wrapper
