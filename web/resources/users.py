import json
from bson import json_util

from ..import dbapplogic


def get_user(username, must_exist=False, db_api=None):
    db_api = db_api or dbapplogic.get_user

    doc = db_api(username)

    if doc and isinstance(doc, dict):
        return doc

    if must_exist:
        raise UserDoesNotExistException(username=username)

    return {}


def extract_balance(user):
    # delete?
    if not user:
        return 0

    if 'balance' not in user:
        return 0

    return user['balance']


def can_add_balance(current_balance, delta):
    if (current_balance + delta) < 0:
        return False

    return True


def update_user_balance(user, delta, db_api=None, get_user_api=None):
    db_api = db_api or dbapplogic.update_user_balance
    return _perform_balance_altering_task(user, delta, db_api, get_user_api)


def _perform_balance_altering_task(user, amt, db_api, get_user_api=None):

    get_user_api = get_user_api or get_user

    if not get_user_api(user['username'], must_exist=True):
        raise UserDoesNotExistException(username=user['username'])

    current_balance = extract_balance(user)

    if not can_add_balance(current_balance, amt):
        raise UserNegativeBalanceException(
            username=user['username'], amt=amt)

    updated_user = db_api(user['username'], amt, user['balance'])
    if not updated_user:
        raise UserBalanceInconsistentException(
            username=user['username'], cached_balance=user['balance'])

    return updated_user['balance']


def create_wager(user, wager_amt, db_api=None, get_user_api=None):
    db_api = db_api or dbapplogic.record_user_wager
    return _perform_balance_altering_task(
            user, wager_amt, db_api, get_user_api)


def extract_history(user):
    # delete?
    if not user:
        return []

    if 'history' not in user:
        return []

    return [json.loads(json_util.dumps(x)) for x in user['history']]


class UserDoesNotExistException(Exception):
    def __init__(self, msg=None, username=None):
        super(UserDoesNotExistException, self).__init__(msg)
        self.username = username


class UserNegativeBalanceException(Exception):
    def __init__(self, msg=None, username=None, amt=None):
        super(UserNegativeBalanceException, self).__init__(msg)
        self.username = username
        self.amt = amt

class UserBalanceInconsistentException(Exception):
    def __init__(self, msg=None, username=None, cached_balance=None):
        super(UserBalanceInconsistentException, self).__init__(msg)
        self.username = username
        self.cached_balance = cached_balance
