import datetime

import pymongo
from pymongo.collection import ReturnDocument

USER = 'eaguayo'
DATA_BASE_NAME = 'interview'


def _get_db_connection():
    client = pymongo.MongoClient()
    return client[DATA_BASE_NAME]


def get_user(username):
    collection = _get_db_connection()['users']
    doc = collection.find_one({"username": username})
    return doc


def update_user_balance(username, delta, current_balance):
    balance_change_type = 'DIPOSITE'
    if delta < 0:
        balance_change_type = 'WIDTHDRAW'

    return _update_user(
        {
            'username': username,
            'balance': {"$eq": current_balance}
        },
        {
            "$inc": {"balance": delta},
            "$push": {"history": {
                "time": datetime.datetime.utcnow(),
                "type": balance_change_type,
                "delta": delta,
                "old_balance": current_balance,
                "balance": (current_balance + delta)
            }}
        }
    )


def _update_user(query, cmd):
    collection = _get_db_connection()['users']
    doc = collection.find_one_and_update(
        query,
        cmd,
        return_document=ReturnDocument.AFTER
    )
    return doc


def record_user_wager(username, wager_amt, current_balance):
    return _update_user(
        {
            'username': username,
            'balance': {"$eq": current_balance}
        },
        {
            "$push": {"history": {
                "time": datetime.datetime.utcnow(),
                "type": "WAGER",
                "amt": wager_amt,
                "balance": current_balance
            }}
        }
    )
