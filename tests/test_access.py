import sys
import unittest
sys.path.append("../")

from web.resources.users import get_user, extract_balance, update_user_balance, \
    extract_history, UserDoesNotExistException, can_add_balance, \
    UserNegativeBalanceException, UserBalanceInconsistentException


class TestBalance(unittest.TestCase):

    def test_none_user_is_zero(self):
        self.assertEqual(0, extract_balance(None))

    def test_no_balance_user_is_zero(self):
        self.assertEqual(0, extract_balance({}))

    def test_gets_correct_balance(self):
        balance = 1337
        self.assertEqual(1337, extract_balance({'balance': 1337}))

    def test_increase_balance(self):
        self._test_balance_change(10)

    def _test_balance_change(self, delta):
        balance = 55
        expected = balance + delta

        doc = {'username': 'foo', 'balance': balance}

        def balance_update_api(username, d, b):
            doc['balance'] += d
            return doc

        self.assertEqual(expected, update_user_balance(
            doc, delta, db_api=balance_update_api,
            get_user_api=self._get_user_api))

    def _get_user_api(self, username, must_exist=False):
        return {'username': username, 'balance': 45}

    def test_decrease_balance(self):
        self._test_balance_change(-10)

    def test_no_negative_balances(self):
        try:
            update_user_balance({'username': 'foo', 'balance': 10}, -100,
                                get_user_api=self._get_user_api)
        except UserNegativeBalanceException as e:
            self.assertEqual(type(e), UserNegativeBalanceException)
        else:
            self.fail("Expceted to raise exception but did not")

    def test_user_does_not_exists_excepts(self):
        def return_none_for_user(username, must_exist):
            return None

        try:
            update_user_balance(
                {'username': 'foo', 'balance': 10},
                1000,
                get_user_api=return_none_for_user)
        except UserDoesNotExistException as e:
            self.assertEqual(type(e), UserDoesNotExistException)
        else:
            self.fail("Expceted to raise exception but did not")

    def test_user_balance_mismatch_excepts(self):

        def return_none_on_balance_change(username, amt, balance=10):
            None

        try:
            update_user_balance(
                {'username': 'foo', 'balance': 20},
                1000,
                get_user_api=self._get_user_api,
                db_api=return_none_on_balance_change)
        except UserBalanceInconsistentException as e:
            self.assertEqual(type(e), UserBalanceInconsistentException)
        else:
            self.fail("Expceted to raise exception but did not")

    def test_can_add_balance_fails(self):
        self.assertFalse(can_add_balance(10, -11))

    def test_can_add_balance_successeds(self):
        self.assertTrue(can_add_balance(100, 99))


class TestUser(unittest.TestCase):

    def test_non_dict_will_be_dict(self):
        def returns_array(username):
            return []

        result = get_user('foo', db_api=returns_array)

        self.assertEqual(type(result), dict)

    def test_non_dict_will_be_empty(self):
        def returns_string(username):
            return "moose"

        result = get_user('foo', db_api=returns_string)

        self.assertEqual(0, len(result))

    def test_dict_returned(self):
        expected = {'a': 1, 'b': 2}

        def returns_dict(username):
            return expected

        result = get_user('foo', db_api=returns_dict)

        # note: all is done by reference, therefor they
        # should be the extact same objects if code working
        # as expected
        self.assertEqual(expected, result)

    def test_required_raises_error(self):
        try:
            def returns_none(foo):
                return None

            get_user('foo', db_api=returns_none, must_exist=True)
        except Exception as e:
            self.assertEqual(type(e), UserDoesNotExistException)
        else:
            self.fail('Expected to raise exception but do not')


class TestHistory(unittest.TestCase):

    def test_none_user_is_array(self):
        self.assertEqual(type(extract_history(None)), list)

    def test_none_user_is_empty(self):
        self.assertEqual(len(extract_history(None)), 0)

    def test_no_history_user_is_empty(self):
        self.assertEqual(type(extract_history({})), list)

    def test_no_history_user_is_empty(self):
        self.assertEqual(len(extract_history(None)), 0)

    def test_gets_correct_history(self):
        history = [2, 4, 5]
        self.assertEqual(history, extract_history({'history': history}))
