# To run
## Create a virtual environment and install the required dependencies
```
virtualenv -p python3 venv
source venv/bin/active
pip install -r requirements.txt
python app.py
```

## Run a local MongoDB
Install mongoDB and run `mongod`, the app assumes its connecting to the default local host.

If on a mac use `brew install mongodb` to install the database.

# Testing
```
cd tests/
pytest
```

# Summary
I wanted to get practice with functional style python programming in the context of a web. I choose the CherryPy web framework because of familiarity and its light weight nature. I was interested in seeing what type of problems would emerge as I implement the functionality for a simple REST API such as how TDD was better/worse/the same in a functional world.

I wanted the app to have state that could change, so I choose to use MongoDB as a datastore. Given the app is suppose to mimic increasing and decrease a users balance (and placing wagers) I had to work around the fact that MongoDB does not offer atomic actions across two documents, but will within one, to ensure all transactions were recorded accurately.

# Observations
* b85e7472c2f02645bbfe79058d995f74f6400100 - Noticed that with functional you have to do something with errors that happen deep down inside the function stack, likely initially failing to fetch a user in the first place. My initial thought, without any research, is to raise exceptions and have the handler deal with all possible exceptions in a try/catch.
* b85e7472c2f02645bbfe79058d995f74f6400100 - At this point, because there is no way to know we failed to get a 0 vs getting a 0 the API is broken. Therefor I decide to raise an exception if the user cannot be found. An error will be returned if this is the case.
* 6609e2b430b37806f900ff81dff42601c70e0729 - Now we have the API working for balance reads and writes and raising all kinds of good errors. Something I notice is that keep each function testable is a design challenge (but a good one). Updating a balance, for example, requires a user to exist and that user to be read. So do we have the balance updater check that first or just try and fail? To keep the the SRP I choose to drop the pre-condition, its the job of someone else to supply a cached user.
* b58f2b6fcff3faa7f4a0af46156e455cf82076f5 - at this point I had hooked up the wager and the history end points. Common errors that we basically ok, such as user does not exist or insufficient funds, kept being raised and all the end points had to deal with these errors. The web API should be standard (e.g., a none existing user should always return a standard error response no matter which end point raised it) and there was a lot of copy paste. I introduced a decorator to catch all reasonable errors and return some standard error message. This was chosen because there was not life-cycle even per request that would capture errors.
* 9bd6b29f58856df5ed0c27d8b46b4018ffd10b1d - after some research decided that checking if the user had the previously cached balance was the best way to deal with transactions. This strategy has the benefit of only updating the users balance if they match their cached copy's balance. The down side is now its impossible to distinguish a user that does not exist vs. a user with insufficient funds. Using this implementation it would require the caller to handle the case where the balance no longer matches and decide to re-try by pulling a full copy of the user and submitting the transaction again. Is this TOO harsh for adding funds though? A: If we want accurate balances recored in the history, yes.
* 4a407a1e518b0a9a9d08cf07d3965466f782e56e - decided that before doing the balance operation the application will check that the user exists and raise its own error. This results in two database calls when the later would have guaranteed one and its not fool proof, in the time between the read and the write the user's balance could be changed, but it seemed acceptable if knowing the balance of the user was critical.

# Research
* Is try/catch and raising errors in general consistent with functional programming styles?
