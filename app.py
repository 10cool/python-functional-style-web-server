import cherrypy
from web.handlers.balance import Balance
from web.handlers.wagers import Wagers
from web.handlers.history import History


class Root(object):
    exposed = True

    def GET(self):
        return 'OK'


class ApiRoot(object):
    exposed = True

    def GET(self):
        return {'classname': self.__class__.__name__}


app = Root()
app.api = ApiRoot()
app.api.balance = Balance()
app.api.wagers = Wagers()
app.api.history = History()

config = {
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher()
    },
    '/api': {
        'tools.json_in.on': True,
        'tools.json_out.on': True
    }
}

cherrypy.quickstart(app, config=config)
